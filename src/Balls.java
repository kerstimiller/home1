public class Balls {

   enum Color {green, red}

   public static void main (String[] param) {
      // for debugging
   }

   public static void reorder (Color[] balls) {
      // TODO!!! Your program here
      int countRed = 0;

      for (int i = 0;  i < balls.length; i++) { // count red balls
         if (balls[i] == Color.red) {
            countRed++;
         }
      }

      if (balls.length != 0) { //array is filled
         if (countRed != 0 && countRed != balls.length) { // array has both color balls

            // fill array with red balls, rest fill with green
            for (int i=0; i < countRed; i++) {
               balls[i] = Color.red;
            }
            for (int i=countRed; i < balls.length; i++) {
               balls[i] = Color.green;
            }
         }
      }
   }
}